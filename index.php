<?php
	get_header();
?>
	<div class="banner_area devider" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/images/banner.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 550px;">
		<div class="container">
			<div class="banner_content">
				<div class="col-md-6">
					<div class="banner_text">
						至極の「リゾート会員ホテル」 <br />
						カンタンに出会える
					</div>
					<div class="search_query_area">
						<div class="search_box">
							<h2 class="search_header">ブランドから探す</h2>
							<div class="search_body">
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /></a></li>
								<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /></a></li>
							</div>
						</div>
					</div>
					<div class="search_query_area area2">
						<div class="search_box">
							<h2 class="search_header">ブランドから探す</h2>
							<div class="search_body">
								<li><a href="">100~200万円</a></li>
								<li><a href="">100~200万円</a></li>
								<li><a href="">100~200万円</a></li>
								<li><a href="">100~200万円</a></li>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="main_form">
						<div class="form_box">
							<div class="form_header">
								ブランドから探す ブランドから探す ブランドから探す
							</div>
							
							<div class="fom_body">
								<div class="single_row">
									<div class="col-md-3">
										お名前
									</div>
									<div class="col-md-9">
										<div class="col-md-6">
											<input type="" placeholder="" />
										</div>
										<div class="col-md-6">
											<input type="" placeholder="" />
										</div>
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										フリガナ
									</div>
									<div class="col-md-9">
										<div class="col-md-6">
											<input type="" placeholder="" />
										</div>
										<div class="col-md-6">
											<input type="" placeholder="" />
										</div>
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										郵便番号
									</div>
									<div class="col-md-9">
										<input type="" placeholder="" />
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										ご住所
									</div>
									<div class="col-md-9">
										<input type="" placeholder="" />
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										電話番号
									</div>
									<div class="col-md-9">
										<input type="" placeholder="" />
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										E-Mail
									</div>
									<div class="col-md-9">
										<input type="" placeholder="" />
									</div>
								</div>
								<div class="single_row">
									<div class="col-md-3">
										希望資料
									</div>
									<div class="col-md-9">
										<input type="" placeholder="" />
									</div>
								</div>
								<div class="text-center">
									<a href="" class="form_btn">入力内容を確認する</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Col Md 9 div -->
	<div class="main_content_area devider" style="padding: 50px 0px">
		<div class="container">
			<div class="col-md-9">
				<div class="single_content_box">
					<div class="content_header_unique">人気リゾートホテルランキング</div>
						
					<div class="carousel_area">
						<div class="owl-carousel owl-theme">

					<?php $resort = new WP_Query(array(
						'post_type' => 'Resorts',
						'post_per_Page' => 6,

						));
					 ?> 
              		<?php while ( $resort->have_posts() ) : $resort->the_post(); ?>
						<div class="item">
						<div class="single_carouesel">
							
								<a href="<?php the_permalink(); ?>">
									<?php echo the_post_thumbnail(); ?>
										
							</a>
								<div class="crsl_title">
									<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								</div>
								<div class="ratings">
									<ul>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<span style="color: red;font-weight: 700">4.9</span>
									</ul>
									
								</div>
								<div class="times">
									一口：<span>120</span> 万円〜
								</div>

								<div class="butttons">
									<ul>
										<li><a >預託性</a></li>
										<li><a >温泉</a></li>
										<li><a >ゴルフ</a></li>
									</ul>
								</div>

								<!-- kisu Show Kore Na -->
								<div class="main_btn">
									<a href="<?php the_permalink(); ?>" class="final_btn">資料を取り寄せる</a>
								</div>
							</div>
						</div>	
							<?php wp_reset_postdata(); endwhile; ?>
					</div><!--Carousel area end-->
				</div><!-- Single Carousel content box End-->

				<div>
				<div class="single_content_box">
					<div class="content_header_unique">エリアから探す</div>
					<div class=" map_box">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/maps.jpg" alt="" />
					</div>
				</div><!-- Single Maps content box End-->
				</div>
				<div class="single_content_box">
					<div class="cta_box">
						<div class="cta_img">
							<a href="<?php 
								redux_options_show('HomePage-banner-link',"#");
							?>">

							<?php if( isset( $redux_values['HomePage-banner-image'] ) ) : ?>
								<img src="<?php redux_options_show('HomePage-banner-image'); ?>" >
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cta_banner.jpg" alt="" />
							<?php endif; ?>

							</a>
						</div>
						<div class="cta_content">
							<div class="cta_header">
								<?php redux_options_show('HomePage-banner-title',"資料請求で、もれなくダウンロード！"); ?>
							</div>
							<div class="cta_btn_class">
								<span class="small_btn"><?php
									redux_options_show('HomePage-button-tagline',"かんたん30秒！");
								?></span>
								<a href="<?php
									redux_options_show('HomePage-button-link',"#");
								?>">
								<?php
									redux_options_show('HomePage-button-text',"資料請求する(無料)");
								?></a>
							</div>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				
<?php if( isset( $redux_values['HomePagePostSection-one-show'] )&& $redux_values['HomePagePostSection-one-show'] == 1 ) :  ?>
				<!--  Area 1 Start--->
				
				<div class="single_content_box">
					<div class="header_diff" style="<?php
						if( isset( $redux_values['HomePagePostSection-one-titleBg'] ) ) {
							echo 'background-color:';
							redux_options_show('HomePagePostSection-one-titleBg');
							echo ';';
						}
					?>">
					<?php
						if( isset( $redux_values['HomePagePostSection-one-title'] ) ) {
							redux_options_show('HomePagePostSection-one-title');
						} else{
							echo "会員リゾートホテルの賢い購入ノウハウ";
						}
					?>
					</div>
					<div class="blog_area">
						<div class="row">
						<?php
						$queryVal = array(
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 6,
						);

						if( isset($redux_values['HomePagePostSection-one-category']) ){
							$queryVal['cat']	= $redux_values['HomePagePostSection-one-category'];
						}

						$query_post = new WP_Query($queryVal);
						while ( $query_post->have_posts() ) : $query_post->the_post();
						?>
							<div class="col-md-4" style="margin-bottom: 20px">
								<a href="<?= the_permalink(); ?>">
									<div class="single_blog">
										
										<div class="blog_thumb">
											<?php echo get_the_post_thumbnail( $_post->ID, 'thumbnail'); ?>
											
										</div>
										<div class="blog_content">
											<div class="blog_title">
												<?= the_title(); ?>
											</div>
											<div class="blog_desc">
												<?= the_content(); ?>
											</div>
											<div class="blog_posted">
												<?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						
						<?php
							endwhile;
						?>
						</div>
						<div class="see_more row">
							<br />
							<a href="<?php
							if( isset($redux_values['HomePagePostSection-one-category'] ) ){
								echo get_category_link($redux_values['HomePagePostSection-one-category']); 
							}
							?>" class="pull-right">もっと見る >>  >></a>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				<!-- Area 1 end -->
<?php endif; ?>				
<?php if( isset( $redux_values['HomePagePostSection-two-show'] )&& $redux_values['HomePagePostSection-two-show'] == 1 ) :  ?>
				<!--  Area 2 Start--->
				
				<div class="single_content_box">
					<div class="header_diff" style="<?php
						if( isset( $redux_values['HomePagePostSection-two-titleBg'] ) ) {
							echo 'background-color:';
							redux_options_show('HomePagePostSection-two-titleBg');
							echo ';';
						}else {
							echo "background: #9E8758;";
						}
					?>">
					<?php
						if( isset( $redux_values['HomePagePostSection-two-title'] ) ) {
							redux_options_show('HomePagePostSection-two-title');
						} else{
							echo "会員リゾートホテルの賢い購入ノウハウ";
						}
					?>
					</div>
					<div class="blog_area">
						<div class="row">
						<?php
						$queryVal = array(
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 6,
						);

						if( isset($redux_values['HomePagePostSection-two-category']) ){
							$queryVal['cat']	= $redux_values['HomePagePostSection-two-category'];
						}

						$query_post = new WP_Query($queryVal);
						while ( $query_post->have_posts() ) : $query_post->the_post();
						?>
							<div class="col-md-4" style="margin-bottom: 20px">
								<a href="<?= the_permalink(); ?>">
									<div class="single_blog">
										
										<div class="blog_thumb">
											<?php echo get_the_post_thumbnail( $_post->ID, 'thumbnail'); ?>
											
										</div>
										<div class="blog_content">
											<div class="blog_title">
												<?= the_title(); ?>
											</div>
											<div class="blog_desc">
												<?= the_content(); ?>
											</div>
											<div class="blog_posted">
												<?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						
						<?php
							endwhile;
						?>
						</div>
						<div class="see_more row">
							<br />
							<a href="<?php
							if( isset($redux_values['HomePagePostSection-two-category'] ) ){
								echo get_category_link($redux_values['HomePagePostSection-two-category']); 
							}
							?>" class="pull-right">もっと見る >>  >></a>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				<!-- Area 2 end -->
<?php endif; ?>				
<?php if( isset( $redux_values['HomePagePostSection-three-show'] )&& $redux_values['HomePagePostSection-three-show'] == 1 ) :  ?>
				<!--  Area 3 Start--->
				
				<div class="single_content_box">
					<div class="header_diff" style="<?php
						if( isset( $redux_values['HomePagePostSection-three-titleBg'] ) ) {
							echo 'background-color:';
							redux_options_show('HomePagePostSection-one-titleBg');
							echo ';';
						}
					?>">
					<?php
						if( isset( $redux_values['HomePagePostSection-three-title'] ) ) {
							redux_options_show('HomePagePostSection-three-title');
						} else{
							echo "会員リゾートホテルの賢い購入ノウハウ";
						}
					?>
					</div>
					<div class="blog_area">
						<div class="row">
						<?php
						$queryVal = array(
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 6,
						);

						if( isset($redux_values['HomePagePostSection-three-category']) ){
							$queryVal['cat']	= $redux_values['HomePagePostSection-three-category'];
						}

						$query_post = new WP_Query($queryVal);
						while ( $query_post->have_posts() ) : $query_post->the_post();
						?>
							<div class="col-md-4" style="margin-bottom: 20px">
								<a href="<?= the_permalink(); ?>">
									<div class="single_blog">
										
										<div class="blog_thumb">
											<?php echo get_the_post_thumbnail( $_post->ID, 'thumbnail'); ?>
											
										</div>
										<div class="blog_content">
											<div class="blog_title">
												<?= the_title(); ?>
											</div>
											<div class="blog_desc">
												<?= the_content(); ?>
											</div>
											<div class="blog_posted">
												<?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						
						<?php
							endwhile;
						?>
						</div>
						<div class="see_more row">
							<br />
							<a href="<?php
							if( isset($redux_values['HomePagePostSection-three-category'] ) ){
								echo get_category_link($redux_values['HomePagePostSection-three-category']); 
							}
							?>" class="pull-right">もっと見る >>  >></a>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				<!-- Area 3 end -->
<?php endif; ?>				
<?php if( isset( $redux_values['HomePagePostSection-four-show'] )&& $redux_values['HomePagePostSection-four-show'] == 1 ) :  ?>
				<!--  Area 4 Start--->
				
				<div class="single_content_box blog_start2">
					<div class="header_diff" style="<?php
						if( isset( $redux_values['HomePagePostSection-four-titleBg'] ) ) {
							echo 'background-color:';
							redux_options_show('HomePagePostSection-four-titleBg');
							echo ';';
						}else {
							echo "background: #9E8758;";
						}
					?>">
					<?php
						if( isset( $redux_values['HomePagePostSection-four-title'] ) ) {
							redux_options_show('HomePagePostSection-four-title');
						} else{
							echo "会員リゾートホテルの賢い購入ノウハウ";
						}
					?>
					</div>
					<div class="blog_area">
						<div class="row">
						<?php
						$queryVal = array(
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 6,
						);

						if( isset($redux_values['HomePagePostSection-four-category']) ){
							$queryVal['cat']	= $redux_values['HomePagePostSection-four-category'];
						}

						$query_post = new WP_Query($queryVal);
						while ( $query_post->have_posts() ) : $query_post->the_post();
						?>
							<div class="col-md-4" style="margin-bottom: 20px">
								<a href="<?= the_permalink(); ?>">
									<div class="single_blog">
										
										<div class="blog_thumb">
											<?php echo get_the_post_thumbnail( $_post->ID, 'thumbnail'); ?>
											
										</div>
										<div class="blog_content">
											<div class="blog_title">
												<?= the_title(); ?>
											</div>
											<div class="blog_desc">
												<?= the_content(); ?>
											</div>
											<div class="blog_posted">
												<?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						
						<?php
							endwhile;
						?>
						</div>
						<div class="see_more row">
							<br />
							<a href="<?php
							if( isset($redux_values['HomePagePostSection-four-category'] ) ){
								echo get_category_link($redux_values['HomePagePostSection-four-category']); 
							}
							?>" class="pull-right">もっと見る >>  >></a>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				<!-- Area 4 end -->
<?php endif; ?>				
<?php if( isset( $redux_values['HomePagePostSection-five-show'] )&& $redux_values['HomePagePostSection-five-show'] == 1 ) :  ?>
				<!--  Area 5 Start--->
				
				<div class="single_content_box">
					<div class="header_diff" style="<?php
						if( isset( $redux_values['HomePagePostSection-five-titleBg'] ) ) {
							echo 'background-color:';
							redux_options_show('HomePagePostSection-five-titleBg');
							echo ';';
						}
					?>">
					<?php
						if( isset( $redux_values['HomePagePostSection-five-title'] ) ) {
							redux_options_show('HomePagePostSection-five-title');
						} else{
							echo "会員リゾートホテルの賢い購入ノウハウ";
						}
					?>
					</div>
					<div class="blog_area">
						<div class="row">
						<?php
						$queryVal = array(
							'post_type' 		=> 'post',
							'posts_per_page' 	=> 6,
						);

						if( isset($redux_values['HomePagePostSection-five-category']) ){
							$queryVal['cat']	= $redux_values['HomePagePostSection-five-category'];
						}

						$query_post = new WP_Query($queryVal);
						while ( $query_post->have_posts() ) : $query_post->the_post();
						?>
							<div class="col-md-4" style="margin-bottom: 20px">
								<a href="<?= the_permalink(); ?>">
									<div class="single_blog">
										
										<div class="blog_thumb">
											<?php echo get_the_post_thumbnail( $_post->ID, 'thumbnail'); ?>
											
										</div>
										<div class="blog_content">
											<div class="blog_title">
												<?= the_title(); ?>
											</div>
											<div class="blog_desc">
												<?= the_content(); ?>
											</div>
											<div class="blog_posted">
												<?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						
						<?php
							endwhile;
						?>
						</div>
						<div class="see_more row">
							<br />
							<a href="<?php
							if( isset($redux_values['HomePagePostSection-five-category'] ) ){
								echo get_category_link($redux_values['HomePagePostSection-five-category']); 
							}
							?>" class="pull-right">もっと見る >>  >></a>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
				<!-- Area 1 end -->
<?php endif; ?>



				
				<div class="single_content_box">
					<div class="cta_box">
						<div class="cta_img">
							<a href="<?php 
								if( isset( $redux_values['HomePage-banner-link'] ) ) {
									redux_options_show('HomePage-banner-link');
								} else{
									echo "#";
								} 
							?>">

							<?php if( isset( $redux_values['HomePage-banner-image'] ) ) : ?>
								<img src="<?php redux_options_show('HomePage-banner-image'); ?>" >
							<?php else: ?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cta_banner.jpg" alt="" />
							<?php endif; ?>

							</a>
						</div>
						<div class="cta_content">
							<div class="cta_header">

							<?php if( isset( $redux_values['HomePage-banner-title'] ) ) : ?>
								<?php redux_options_show('HomePage-banner-title'); ?>
							<?php else: ?>
								資料請求で、もれなくダウンロード！
							<?php endif; ?>

							</div>
							<div class="cta_btn_class">
								<span class="small_btn"><?php
									if( isset( $redux_values['HomePage-button-tagline'] ) ) {
										redux_options_show('HomePage-button-tagline');
									} else{
										echo "かんたん30秒！";
									}
								?></span>
								<a href="<?php
									if( isset( $redux_values['HomePage-button-link'] ) ) {
										redux_options_show('HomePage-button-link');
									} else{
										echo "#";
									}
								?>">
								<?php
									if( isset( $redux_values['HomePage-button-text'] ) ) {
										redux_options_show('HomePage-button-text');
									} else{
										echo "資料請求する(無料)";
									}
								?></a>
							</div>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
			</div>
		</div>

		<?php
				get_sidebar();
			?>
	</div>
</div>
	
	
<?php
	get_footer();
?>
	
