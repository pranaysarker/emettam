<?php
	get_header();
?>
	
	<div class="main_content_area devider" style="padding: 50px 0px">
		<div class="container">
			<div class="col-md-9">
				<div class="section_content_area">
					<div class="section_area_area">
						<h1 class="">エリアから探す：沖縄</h1>
					</div>
					<div class="ovrview_btn_and_text">
						<div class="col-md-6 col-sm-6">
							<div class="ovrview_text">
								募集価格：<span style="color: #f23131">462</span>万円（１口/税込）
							</div>
						</div>
						<div class="col-md-6  col-sm-6">
							<div class="single-content_btn" style="margin-top: 0px">
								<a href="" style="min-width: 350px">当物件の資料請求はこちら(無料)</a>
							</div>
						</div>
					</div>
					<div class="ovrview-area">
						<div class="overview_body">
							<div class="over_view_header">
								<ul>
									<li class=""><a href="">外観・共用施設</a></li>
									<li class="ovractive"><a href="">募集概要</a></li>
									<li><a href="">評価・口コミ</a></li>
								</ul>
							</div>
							<div class="overview_content">
								<div class="overview_content_highlight">
									東急ハーヴェストクラブ 蓼科リゾートの概要
								</div>
								
								<div class="overview_cotent_table">
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											仲介価格
										</div>
										<div class="overview_single_content_right">
											1,150万円（税込） <br/>
											※仲介物件につき、仲介手数料が別途必要になります。<br/>
											※仲介価格は更新日現在の価格であり、相場の変更や売却済みの際はご容赦ください。
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											<span class="pull-left" style="color: #535252">内訳</span> 仲介価格
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg border_top_bottom">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											 仲介価格
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left ">
											会員権利金
										</div>
										<div class="overview_single_content_right">
											7,000円
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											取引態様
										</div>
										<div class="overview_single_content_right border_top_bottom">
											仲介/東急リゾート（株）
										</div>
									</div>
								</div>
								<div class="overview_content_highlight">
									東急ハーヴェストクラブ 蓼科リゾートの概要
								</div>
								<div class="overview_cotent_table">
									
									
									
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg border_top_bottom">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											7,000口
										</div>
									</div>
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											総募集口数
										</div>
										<div class="overview_single_content_right border_top_bottom">
											
										</div>
									</div>
									
									<div class="overview_single_content">
										<div class="overview_single_content_left bg">
											取引態様
										</div>
										<div class="overview_single_content_right border_top_bottom">
											仲介/東急リゾート（株）
										</div>
									</div>
								</div>
							</div>
								
							<div class="overview_footer">
								※年会費・固定資産税については所有月数により、売主と月割清算となります。
							</div>
						</div>
					</div>
				</div>
				<!-- Area 3 end -->
				
				<div class="single_content_box">
					<div class="cta_box">
						<div class="cta_img">
							<a href=""><img src="<?= get_template_directory_uri(); ?>/assets/images/cta_banner.jpg" alt="" /></a>
						</div>
						<div class="cta_content">
							<div class="cta_header">
								資料請求で、もれなくダウンロード！
							</div>
							<div class="cta_btn_class">
								<span class="small_btn">かんたん30秒！</span>
								<a href="">資料請求する(無料)</a>
							</div>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
			</div>
			<!---
			*
			*
				Sidebar 
			*
			*
			-->
			<?php
				get_sidebar();
			?>
		</div>
	</div>
	
	
<?php
	get_footer();
?>
	
