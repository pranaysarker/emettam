<?php 
	// $options = get_option( 'emettam-option' ); // unique id of the framework
	// echo $options['topbartext']; 

	// $topbartext =$options['topbartext'];
	global $redux_values;
?>



<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<style type="text/css">
		@font-face {
		font-family: hirakupro;
		src: url("<?php echo get_template_directory_uri(); ?>/assets/fonts/hirakakupro-w3-opentype.otf") /* EOT file for IE */
	}
	</style>
	<?php wp_head();?>
</head>
<body <?php body_class(); ?>>
	<div class="header_area devider">
		<div class="container">
			<div class="row">
				<div class="text_with_10px_padding">
					<?php  redux_options_show('headerTopBar-text'); ?>
				</div>
			</div>
			
		</div>

		<!-- Advanced Search End -->
		<div class="logo_and_banner_area">
			<div class="container">
				<div class="col-md-5 col-sm-5">
					<div class="logo">
						<a href="<?= site_url(); ?>">
							<?php
							// var_dump($redux_values);
							$header_image = $redux_values["header-logo-image"]['url'];
							if( !empty( $header_image ) ){
								echo "<img src='";
								redux_options_show('header-logo-image');
								echo "' >";
							}else {
								redux_options_show('header-logo-text','リゾート会員ホテル探し.com'); 
							}
							?>
						</a>
						<span class="mobile_nav_toggle">
							<a href="" class="nav_toggle_tgl"><i class="fa fa-bars"></i></a>
							<a href="" class="nav_toggle nav_toggletext">お気に 入り</a>
						</span>
					</div>
						
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="header_banner ">
							<h2><?php redux_options_show('header-logo-banner-text','資料請求で、もれなくダウンロード！'); ?></h2>
							<a href="">
								<img src="<?php redux_options_show('header-logo-banner-image',get_theme_file_uri('/assets/images/header_banner.jpg') ); ?>" alt="" />
							</a>
						</div>
					</div>
			</div>
		</div>
	</div>
	
	
	<nav class="nav_area devider">
		<div class="container">
			<div class="header_menu">
					<ul>
					<li class="active"><a href="">エリアから探す</a></li>
					<li><a href="">ブランドから探す</a></li>
					<li><a href="">人気ランキング</a></li>
					<li><a href="">初めての方・お役立ち</a></li>
					<li><a href="">お気に入りリスト</a></li>
				</ul>
			
			</div>
		</div>
	</nav>

	
	