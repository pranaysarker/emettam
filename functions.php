<?php

	function emettam_add_theme_support(){
		add_theme_support('title-tag');
		add_theme_support('menus');
		add_theme_support('post-thumbnails');
		add_theme_support( 'custom-logo');
		add_theme_support('post-formats', array('aside','image','video'));

		// Resister Nav Menu Here

		register_nav_menu('primary','Primary header menu');
		
		register_nav_menus(
			array(
			'primary-menu' => __( 'Primary Menu' ),
			'secondary-menu' => __( 'Secondary Menu' )
			)
		);
	}
	
	add_action('after_setup_theme', 'emettam_add_theme_support');
	
	
	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

	function special_nav_class ($classes, $item) {
		if (in_array('current-page-ancestor', $classes) || in_array('current-menu-item', $classes) ){
			$classes[] = 'active ';
		}
		return $classes;
	}

	// Style And Script Enqueuee Emettam Here

 function Emettam_style_scripts() {
	// enqueue For style
	wp_enqueue_style('font-awesome',get_theme_file_uri('assets/css/font-awesome.min.css'));
	wp_enqueue_style('bootstrap',get_theme_file_uri('assets/css/bootstrap.min.css'));
	wp_enqueue_style('owl.carousel', get_theme_file_uri('assets/css/owl.carousel.min.css'));
	wp_enqueue_style('style',get_theme_file_uri('style.css'));
    wp_enqueue_style( 'main-stylesheet', get_stylesheet_uri() );
    wp_enqueue_style('responsive',get_theme_file_uri('assets/css/responsive.css'));

    // enqueue For Scripts

        wp_enqueue_script( 'bootstrap', get_theme_file_uri('assets/js/bootstrap.min.js'), array('jquery'), '', true );

    wp_enqueue_script( 'sowl.carousel', get_theme_file_uri('assets/js/owl.carousel.min.js'), array('jquery'), '', true );

    wp_enqueue_script( 'custom', get_theme_file_uri('assets/js/custom.js'), array('jquery'), ' ', true );
	}
add_action( 'wp_enqueue_scripts', 'Emettam_style_scripts' );

/**
 * All included files
 */
// Register our sidebars and widgetized areas.

include(__DIR__."/inc/widget/widget-adding.php");

// custom post types
include(__DIR__.'/lib/emettamcustompost.php');
// include(__DIR__.'/inc/emettam-option/codestar-framework.php');
// include(__DIR__.'/inc/emettam-option/emettam-options.php');

include(__DIR__.'/lib/redux-framework-master/redux-framework.php');
include(__DIR__.'/lib/redux-framework-master/sample/config.php');


// helper functions 
include(__DIR__."/inc/functions-helper.php");


		