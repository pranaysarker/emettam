	<!--   Footer Section Start    -->
	
	<footer class="footer_area devider">
		<div class="container">
			<div class="footer_menu_area">
				<?php dynamic_sidebar('footer_sidebar'); ?>
				<!-- <div class="col-md-3">
					<div class="footer_header">
						条件検索
					</div>
					
					<ul>
						<li><a href="">人気ランキング</a></li>
						<li><a href="">エリアから探す</a></li>
						<li><a href="">ブランドから探す</a></li>
						<li><a href="">価格から探す</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<div class="footer_header">
						初めての方
					</div>
					
					<ul>
						<li><a href="">初めての方お役立ちガイド</a></li>
						<li><a href="">会員リゾートホテルの賢い購入ノウハウ</a></li>
						<li><a href="">人気コラム記事</a></li>
						<li><a href="">体験レポート</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<div class="footer_header">
						お問い合わせ
					</div>
					
					<ul>
						<li><a href="">資料請求</a></li>
						<li><a href="">お気に入りリスト</a></li>
						<li><a href="">運営会社</a></li>
						<li><a href="">プライバシーポリシー</a></li>
						<li><a href="">利用規約</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<div class="footer_header">
						
					</div>
					<ul>
						<li><a href="">物件掲載のお問い合わせ</a></li>
					</ul>
				</div> -->
			</div>
			
			<div class="footer_text_area">
				<p>
					<?php
						redux_options_show('Footer-para-text','◆当サイトに関する注意事項当サイトで提供する商品の情報にあたっては、十分な注意を払って提供しておりますが、情報の正確性その他一切の事項について保証をするものではありません。
						◆お申込みにあたっては、提携事業者のサイトや、利用規約をご確認の上、ご自身でご判断ください。
						◆当社では各商品のサービス内容及びキャンペーン等に関するご質問にはお答えできかねます。提携事業者に直接お問い合わせください。
						◆本ページのいかなる情報により生じた損失に対しても当社は責任を負いません。なお、本注意事項に定めがない事項は当社が定める「利用規約」 が適用されるものとします。');
					?>
					

				</p>
			</div>
			
			<div class="copyright_text">
			<?php
				redux_options_show('Footer-copyright','&copy; 2019 リゾート会員ホテルナビ.');
			?>
			</div>
		</div>
	</footer>
	<?php wp_footer();?>
</body>
</html>

