<?php
	get_header();
?>
	<div class="small_header devider" style="background: url('<?php get_template_directory_uri(); ?>/assets/images/banner.jpg');
	background-repeat: no-repeat;
	background-size: cover;">
		<div class="container">
			<div class="banner_content text-center" style="padding: 50px 0px">
				<?php the_title(); ?>
			</div>
		</div>
	</div>
	
	<div class="main_content_area devider" style="padding: 50px 0px">
		<div class="container">
			<div class="col-md-9">
				<div class="single_content_box">
					<?php
						while ( have_posts() ) : the_post();
							the_author();
							the_content();
						endwhile;
					?>
				</div>
			</div>
			<!---
			*
			*
				Sidebar 
			*
			*
			-->
			<?php
				get_sidebar();
			?>
		</div>
	</div>
	
	
<?php
	get_footer();
?>
	
