<?php 

function custom_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home Right Sidebar One',
		'id'            => 'home_right_one',
		'before_widget' => '<div class="cta_box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="cta_header">',
		'after_title'   => '</div>',
    ) );
    
	register_sidebar( array(
		'name'          => 'Home Right Sidebar Two',
		'id'            => 'home_right_two',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<div class="cta_header">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Home Right Sidebar Three',
		'id'            => 'home_right_three',
		'before_widget' => '<div class="cta_box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="cta_header">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => 'Home Right Sidebar Four',
		'id'            => 'home_right_four',
		'before_widget' => '<div class="cta_box">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="cta_header">',
		'after_title'   => '</div>',
	) );
	// footer sidebar 
	register_sidebar( array(
		'name'          => 'Footer Sidebar',
		'id'            => 'footer_sidebar',
		'before_widget' => '<div class="col-md-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="footer_header">',
		'after_title'   => '</div>',
	) );
	

}
add_action( 'widgets_init', 'custom_widgets_init' );


/***
 *  registering widgets
 */
include(__DIR__."/single-widget/resource-file-widget.php");
include(__DIR__."/single-widget/resort-member-widget.php");
include(__DIR__."/single-widget/brand-search-widget.php");
include(__DIR__."/single-widget/brand-search-widget-rs4.php");
include(__DIR__."/single-widget/footer-widget-custom.php");




