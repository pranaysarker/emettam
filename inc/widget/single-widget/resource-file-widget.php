<?php
    
 
class ResourceFileWidget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'RS1: Resource File Widget' );
	}

	function widget( $args, $instance ) {
        echo $args['before_widget'];
        // title 
        echo $args['before_title'];
        echo $instance['title'];
        echo $args['after_title'];
        // title end
        ?>
        <!-- banner  -->
        <div class="cta_img">
            <a href="<?php echo $instance['banner_link']; ?>"><img src="<?php echo $instance['banner_image']; ?>" /></a>
        </div>
        <!-- banner end  -->
        <!-- content -->
        <div class="cta_content">
            <div class="cta_btn_class">
                <span class="small_btn"><?php echo $instance['button_subtitle']; ?></span>
                <a href="<?php echo $instance['button_link']; ?>"><?php echo $instance['button_text']; ?></a>
            </div>
        </div>
        <!-- content end  -->
		<?php
        echo $args['after_widget'];
	}

	function form( $instance ) {
        ?>
        <!-- title  -->
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                Title 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
        </p>
        <!-- banner  -->
        <p>
            <label for="<?php echo $this->get_field_id('banner_link'); ?>">
                Baner Link 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('banner_link');?>" name="<?php echo $this->get_field_name('banner_link'); ?>" value="<?php echo $instance['banner_link']; ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('banner_image'); ?>">
                Banner image link 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('banner_image');?>" name="<?php echo $this->get_field_name('banner_image'); ?>" value="<?php echo $instance['banner_image']; ?>">
        </p>
        <!-- button  -->
        <p>
            <label for="<?php echo $this->get_field_id('button_subtitle'); ?>">
                Button Subtitle 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('button_subtitle');?>" name="<?php echo $this->get_field_name('button_subtitle'); ?>" value="<?php echo $instance['button_subtitle']; ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('button_link'); ?>">
                Button Link 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('button_link');?>" name="<?php echo $this->get_field_name('button_link'); ?>" value="<?php echo $instance['button_link']; ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('button_text'); ?>">
                Button text 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('button_text');?>" name="<?php echo $this->get_field_name('button_text'); ?>" value="<?php echo $instance['button_text']; ?>">
        </p>
        <?php 
    }
    
	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

}

function myplugin_register_widgets() {
	register_widget( 'ResourceFileWidget' );
}

add_action( 'widgets_init', 'myplugin_register_widgets' );