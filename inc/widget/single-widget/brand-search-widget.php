<?php
    
 
class BrandSearchWidgetRS3 extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'RS3: Brand Search Widget' );
	}

	function widget( $args, $instance ) {
        ?>
            <div class="single_widget" >
                <div class="bookmark_widget">
            
                    <div class="bookmark_widget_content" >
                        <div class="bkmrk_left">

                            <?php
                                if( isset( $instance['brand_image'] )){
                                    echo "<img src='".$instance["brand_image"]."' >";
                                }else {
                                    ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" />
                                    <?php 
                                }
                            ?>
                            <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/barnad-1.jpg" alt="" /> -->

                            <div class="bkmrk_head "> <?php echo $instance['title']; ?></div>
                            <div  class="bkmrk_ratings "><span> <?php 
                                if( isset( $instance['brand_rating'] ) ){
                                    $rating_old = $instance['brand_rating'];

                                    if( !is_string( $rating_old ) ){

                                        $langth = round( $rating_old );
                                        for($i=0;$i<$rating_old;$i++){
                                            echo "★";
                                        }

                                    }else {

                                        $rating_new = (float)$rating_old;
                                        $length = round($rating_new);
                                        for($i=0;$i<$length;$i++){
                                            echo "★";
                                        }

                                    }
                                    


                                } 
                            ?> </span><?php echo $instance['brand_rating']; ?></div>
                            <div class="bkmrktimes">ホテル数： <span style="color: red"><?php echo $instance['hotel_number']; ?></span>  箇所</div>
                        </div>
                        <div class="bkmrk_right">
                            <img class="rating-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/star.jpg" alt="" />
                            <img class="hidden-rating-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/star.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        <?php 
	}

	function form( $instance ) {
        ?>
        <!-- brand image  -->
        <p>
            <label for="<?php echo $this->get_field_id('brand_image'); ?>">
                Brand Image 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('brand_image');?>" name="<?php echo $this->get_field_name('brand_image'); ?>" value="<?php echo $instance['brand_image']; ?>">
        </p>
        <!-- title  -->
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                Title 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
        </p>
        <!-- Brand ratting  -->
        <p>
            <label for="<?php echo $this->get_field_id('brand_rating'); ?>">
                Rating Value ( number )
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('brand_rating');?>" name="<?php echo $this->get_field_name('brand_rating'); ?>" value="<?php echo $instance['brand_rating']; ?>">
        </p>
        <!-- Hotel Number  -->
        <p>
            <label for="<?php echo $this->get_field_id('hotel_number'); ?>">
                Hotel Number
            </label>
            <input type="number" class="widefat" id="<?php echo $this->get_field_id('hotel_number');?>" name="<?php echo $this->get_field_name('hotel_number'); ?>" value="<?php echo $instance['hotel_number']; ?>">
        </p>
        
        <?php 
    }
    
	function update( $new_instance, $old_instance ) {
        // $new_instance = $old_instance;
		return $new_instance;
	}

}

function brandSearchWidgetFunc() {
	register_widget( 'BrandSearchWidgetRS3' );
}

add_action( 'widgets_init', 'brandSearchWidgetFunc' );