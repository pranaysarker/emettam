<?php
    
 
class BrandSearchWidgetRS4 extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'RS4: Brand Widget With Image' );
	}

	function widget( $args, $instance ) {
        ?>
            <div class="single_rest_area">
                <div class="left_side_rest">
                    <div class="crown">
                        <img src="http://localhost/worddev-fiverr/wp-content/themes/Emettam/assets/images/crown.jpg" alt="" width="20px">  
                        <?php

                            echo $instance['brand_image'];
                        ?> 
                    </div>
                    <div class="rest_text">
                        <?php echo $instance['brand_image']; ?>
                    </div>
                </div>
                <div class="right_side_rest">
                    
                    <?php $brand_image = $instance['brand_image'];
                        if(isset($brand_image)){
                            echo "<img src='".$brand_image."' />";
                        }else{
                            ?>

                            <img src="http://localhost/worddev-fiverr/wp-content/themes/Emettam/assets/images/carosusel_img.jpg" alt="">

                            <?php 
                        }
                    ?>
                    
                </div>
            </div>
        <?php 
	}

	function form( $instance ) {
        ?>
        <!-- title  -->
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                Title 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
        </p>
        <!-- image  -->
        <p>
            <label for="<?php echo $this->get_field_id('brand_image'); ?>">
                Brand Image 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('brand_image');?>" name="<?php echo $this->get_field_name('brand_image'); ?>" value="<?php echo $instance['brand_image']; ?>">
        </p>
        <!-- Brand content  -->
        <p>
            <label for="<?php echo $this->get_field_id('content'); ?>">
                Brand content
            </label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('content');?>" name="<?php echo $this->get_field_name('content'); ?>"><?php echo $instance['content']; ?></textarea>
        </p>
        
        <?php 
    }
    
	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

}

function BrandSearchWidgetRS4func() {
	register_widget( 'BrandSearchWidgetRS4' );
}

add_action( 'widgets_init', 'BrandSearchWidgetRS4func' );