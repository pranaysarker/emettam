<?php
    
 
class Resort_member_widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'RS2: Resort Member Widget' );
	}

	function widget( $args, $instance ) {
        ?>
            <div class="widget_body" style="padding: ;background: #fff;text-align: center;background: url('<?php echo $instance['banner_image']; ?>');background-repeat: no-repeat;background-size:cover">
                <div class="widget_img_area">
                    <a href="" class="widget_btn">
                    <?php echo $instance['button_text']; ?>
                    </a>
                </div>
                <div class="widget_bottom_header">
                    <?php echo $instance['widget_tagline']; ?>
                </div>
            </div>
        <?php 
	}

	function form( $instance ) {
        ?>
        <!-- title  -->
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                Title 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
        </p>
        <!-- banner  -->
        <p>
            <label for="<?php echo $this->get_field_id('banner_image'); ?>">
                Banner image link 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('banner_image');?>" name="<?php echo $this->get_field_name('banner_image'); ?>" value="<?php echo $instance['banner_image']; ?>">
        </p>
        <!-- button  -->
        <p>
            <label for="<?php echo $this->get_field_id('button_link'); ?>">
                Button Link 
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('button_link');?>" name="<?php echo $this->get_field_name('button_link'); ?>" value="<?php echo $instance['button_link']; ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('button_text'); ?>">
                Button text 
            </label>
            <textarea type="text" class="widefat" id="<?php echo $this->get_field_id('button_text');?>" name="<?php echo $this->get_field_name('button_text'); ?>" ><?php echo $instance['button_text']; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('widget_tagline'); ?>">
                Widget Tagline
            </label>
            <textarea type="text" class="widefat" id="<?php echo $this->get_field_id('widget_tagline');?>" name="<?php echo $this->get_field_name('widget_tagline'); ?>" ><?php echo $instance['widget_tagline']; ?></textarea>
        </p>
        <?php 
    }
    
	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

}

function resort_member_widget_register() {
	register_widget( 'Resort_member_widget' );
}

add_action( 'widgets_init', 'resort_member_widget_register' );