<?php
    
function redux_options_show($value,$fallback=''){
	global $redux_values;

	if( is_array( $redux_values[$value] )){
		if( !empty( $redux_values[$value]['url'] )){
			echo $redux_values[$value]['url'];
		}else {
			echo $fallback;
		}
	}else {
		if( !empty( $redux_values[$value] )) {

			echo $redux_values[$value];

		}else {
			echo $fallback;
		}
	}

}