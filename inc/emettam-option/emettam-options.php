
<?php  

// Control core classes for avoid errors
if( class_exists( 'CSF' ) ) {

  //
  // Set a unique slug-like ID
  $prefix = 'emettam';

  //
  // Create options
  CSF::createOptions( $prefix, array(
    'menu_title' => 'Emettam Option',
    'menu_slug'  => 'emettam-option',
  ) );

  //
  // Create a section
  CSF::createSection( $prefix, array(
    'title'  => 'Header',
    'fields' => array(

      //
      // A text field
    array(
        'id'    => 'topbartext',
        'type'  => 'text',
        'title' => 'Topbar Left Text',
      ),

	array(
		  'id'    => 'header_logo',
		  'type'  => 'media',
		  'title' => 'Upload Logo',
		),

      array(
        'id'    => 'header',
        'type'  => 'text',
        'title' => 'Simple Text',
      ),

    )
  ) );

  //
  // Create a section
  CSF::createSection( $prefix, array(
    'title'  => 'Footer',
    'fields' => array(

      // A textarea field
      array(
        'id'    => 'footer',
        'type'  => 'text',
        'title' => 'Simple text',
      ),

    )
  ) );

}