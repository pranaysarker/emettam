<?php
	get_header();
?>
	
	<div class="main_content_area devider" style="padding: 50px 0px">
		<div class="container">
			<div class="col-md-9">
				<div class="section_content_area">
					<div class="section_area_area">
						<h1 class="">エリアから探す：沖縄</h1>
					</div>
					<div class="section_contents">
						<div class="single_content">
						
							<div class="col-md-5 col-xs-12 col-sm-12 ">
								<div class="img_gal">
									<ul>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
									</ul>
								</div>
							</div> 
							<div class="col-md-7 col-xs-12 col-sm-12 ">
								<div class="single_content_area">
									<h1>箱根リゾート(東急リゾート）</h1>
									<div class="single_time_and_ratings">
										一口：<span style="color: #fe3c3c">120</span> <span>万円〜</span>
										<span>
											<strong style="color: #e2a72a">★★★★★</strong> <strong style="color: #fe3c3c"> 4.9</strong>
										</span>
									</div>
									<div class="list_item_btns">
										<ul>
											<li><a href="">預託性</a></li>
											<li><a href="">温泉</a></li>
											<li><a href="">ゴルフ</a></li>
										</ul>
									</div>
									<div class="single-content_text">
										XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
										XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
									</div>
									<div class="single-content_btn">
										<a href="">資料請求（無料）</a>
									</div>
									
								</div>
							</div>
						
						</div>
						<div class="single_content">
						
							<div class="col-md-5">
								<div class="img_gal">
									<ul>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
										<li><img src="<?= get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" /></li>
									</ul>
								</div>
							</div>
							<div class="col-md-7">
								<div class="single_content_area">
									<h1>箱根リゾート(東急リゾート）</h1>
									<div class="single_time_and_ratings">
										一口：<span style="color: #fe3c3c">120</span> <span>万円〜</span>
										<span>
											<strong style="color: #e2a72a">★★★★★</strong> <strong style="color: #fe3c3c"> 4.9</strong>
										</span>
									</div>
									<div class="list_item_btns">
										<ul>
											<li><a href="">預託性</a></li>
											<li><a href="">温泉</a></li>
											<li><a href="">ゴルフ</a></li>
										</ul>
									</div>
									<div class="single-content_text">
										XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
										XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
									</div>
									<div class="single-content_btn">
										<a href="">資料請求（無料）</a>
									</div>
									
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
			<!---
			*
			*
				Sidebar 
			*
			*
			-->
			<?php
				get_sidebar();
			?>
		</div>
	</div>
	
	
<?php
	get_footer();
?>
	
