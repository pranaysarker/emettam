<?php
	get_header();
?>
	
	<div class="main_content_area devider" style="padding: 50px 0px">
		<div class="container">
			<div class="col-md-9">
				<div class="section_content_area">
					<div class="section_area_area">
						<h1 class="">エリアから探す：沖縄</h1>
					</div>
					<div class="ovrview_btn_and_text">
						<div class="col-md-6 col-sm-6">
							<div class="ovrview_text">
								募集価格：<span style="color: #f23131">462</span>万円（１口/税込）
							</div>
						</div>
						<div class="col-md-6  col-sm-6">
							<div class="single-content_btn" style="margin-top: 0px">
								<a href="" style="min-width: 350px">当物件の資料請求はこちら(無料)</a>
							</div>
						</div>
					</div>
					<div class="ovrview-area">
						<div class="overview_body">
							<div class="over_view_header">
								<ul>
									<li class=""><a href="">外観・共用施設</a></li>
									<li class="ovractive"><a href="">募集概要</a></li>
									<li><a href="">評価・口コミ</a></li>
								</ul>
							</div>
							<div class="overview_content">
								<div class="overview_content_highlight">
									東急ハーヴェストクラブ 蓼科リゾートの概要
								</div>
								<!--  SLider Area --->
								<div class="carousel_area2">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										  <!-- Indicators -->
										  <ol class="carousel-indicators">
											<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
											<li data-target="#carousel-example-generic" data-slide-to="1"></li>
											<li data-target="#carousel-example-generic" data-slide-to="2"></li>
										  </ol>

										  <!-- Wrapper for slides -->
										  <div class="carousel-inner" role="listbox">
											<div class="item active">
											  <img src="<?= get_template_directory_uri(); ?>/assets/images/sliderbg.jpg" alt="...">
											  <div class="carousel-caption">
												
											  </div>
											</div>
											<div class="item">
											  <img src="<?= get_template_directory_uri(); ?>/assets/images/sliderbg.jpg" alt="...">
											  <div class="carousel-caption">
												
											  </div>
											</div>
											
										  </div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
								<!--  SLider Area --->
								<div class="fav-text">
								ラウンジ、レストラン、露天風呂付温泉大浴場※加温・加水・循環ろ過装置、展望家族風呂、スパプール、屋外
								プール、エステサロン、湯上りラウンジ、ゲームルーム、多目的室、駐車場（有料／一滞在、一台につき500円(税込)）
								※ホームグラウンド会員は無料

								</div>
								<div class="overview_content_highlight">
									東急ハーヴェストクラブ 蓼科リゾートの概要
								</div>
								<!--  SLider Area --->
								<div class="carousel_area2">
									<div id="carousel_again" class="carousel slide" data-ride="carousel">
										  <!-- Indicators -->
										  <ol class="carousel-indicators">
											<li data-target="#carousel_again" data-slide-to="0" class="active"></li>
											<li data-target="#carousel_again" data-slide-to="1"></li>
											<li data-target="#carousel_again" data-slide-to="2"></li>
										  </ol>

										  <!-- Wrapper for slides -->
										  <div class="carousel-inner" role="listbox">
											<div class="item active">
											  <img src="<?= get_template_directory_uri(); ?>/assets/images/sliderbg.jpg" alt="...">
											  <div class="carousel-caption">
												
											  </div>
											</div>
											<div class="item">
											  <img src="<?= get_template_directory_uri(); ?>/assets/images/sliderbg.jpg" alt="...">
											  <div class="carousel-caption">
												
											  </div>
											</div>
											
										  </div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel_again" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel_again" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
								<div class="fav-text">
								バリエーション豊富な16タイプ。大切なひとと豊かな休日を共有する「ハーヴェストクラブ」。ゆとりある休日は、
								気持ちのいい海の眺めから始まります。

								</div>
								
								<div class="custom_table">
										<div class="table-responsive myTable">
										  <table class="table">
											<thead>
												<th>部屋タイプ</th>
												<th>標準定員</th>
												<th>最大定員</th>
												<th>客室面積</th>
												<th>客数</th>
											</thead>
											<tbody>
												<tr>
													<td>ツイン</td>
													<td>2名</td>
													<td>2名</td>
													<td>37〜42m2</td>
													<td>16室</td>
												</tr>
												<tr>
													<td>ツイン</td>
													<td>2名</td>
													<td>2名</td>
													<td>37〜42m2</td>
													<td>16室</td>
												</tr>
												<tr>
													<td>ツイン</td>
													<td>2名</td>
													<td>2名</td>
													<td>37〜42m2</td>
													<td>16室</td>
												</tr>
												<tr>
													<td>ツイン</td>
													<td>2名</td>
													<td>2名</td>
													<td>37〜42m2</td>
													<td>16室</td>
												</tr>
												<tr>
													<td>ツイン</td>
													<td>2名</td>
													<td>2名</td>
													<td>37〜42m2</td>
													<td>16室</td>
												</tr>
												
											</tbody>
										  </table>
										</div>
									
								</div>
								
								<div class="overview_content_highlight">
									アクセス・ロケーション
								</div>
								
								<div class="fav-content_area">
									<h2>ロケーション</h2>
								</div>
								<div class="few_map_text">住所：　XXXXXXXXXXXXXXXXXXXXX</div>
								
								<div class="my_map_area">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39883.69363992207!2d-0.7701237481974497!3d51.334470129265185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487429f460990ce1%3A0xa0758862f1c77087!2sCamberley%2C%20UK!5e0!3m2!1sen!2sbd!4v1571034246581!5m2!1sen!2sbd" width="" height="" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
								</div>
								<div class="fav-content_area">
									<h2>アクセス</h2>
								</div>
								
								<div class="stats">
									<div class="col-md-6">
										<h3>電車をご利用の場合</h3>
										<img src="<?= get_template_directory_uri(); ?>/assets/images/stats_1.png" alt="" />
									</div>
									<div class="col-md-6">
										<h3>電車をご利用の場合</h3>
										<img src="<?= get_template_directory_uri(); ?>/assets/images/stats_2.png" alt="" />
									</div>
								</div>
								
							</div>
								
						</div>
					</div>
				</div>
				<!-- Area 3 end -->
				
				<div class="single_content_box">
					<div class="cta_box">
						<div class="cta_img">
							<a href=""><img src="<?= get_template_directory_uri(); ?>/assets/images/cta_banner.jpg" alt="" /></a>
						</div>
						<div class="cta_content">
							<div class="cta_header">
								資料請求で、もれなくダウンロード！
							</div>
							<div class="cta_btn_class">
								<span class="small_btn">かんたん30秒！</span>
								<a href="">資料請求する(無料)</a>
							</div>
						</div>
					</div>
				</div><!-- Single CTA content box End-->
			</div>
			<!---
			*
			*
				Sidebar 
			*
			*
			-->
			<?php
				get_sidebar();
			?>
		</div>
	</div>
	
	
<?php
	get_footer();
?>
	
