		
<?php
// Post Type Events
add_action( 'init', 'Resorts_post', 0 );

function Resorts_post() {

	$labels = array(
		'name'                => _x( 'Resorts', 'Post Type General Name', 'gourmet-artist' ),
		'singular_name'       => _x( 'Resort', 'Post Type Singular Name', 'gourmet-artist' ),
		'menu_name'           => __( 'Resorts', 'emettam' ),
		'parent_item_colon'   => __( 'Parent Resorts ', 'emettam' ),
		'all_items'           => __( 'All Resorts', 'emettam' ),
		'view_item'           => __( 'View Resort', 'emettam' ),
		'add_new_item'        => __( 'Add New Resort', 'emettam' ),
		'add_new'             => __( 'Add New Resort', 'emettam' ),
		'edit_item'           => __( 'Edit Resort', 'emettam' ),
		'update_item'         => __( 'Update Resort', 'emettam' ),
		'search_items'        => __( 'Search Resort', 'emettam' ),
		'not_found'           => __( 'Not Found', 'emettam' ),
		'not_found_in_trash'  => __( 'Not Found in Trash', 'emettam' ),
	);

	$args = array(
		'label'               => __( 'Resorts', 'emettam' ),
		'description'         => __( 'Resorts', 'emettam' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
   		'menu_icon'           => 'dashicons-location-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
	);

	register_post_type( 'Resorts', $args );

}
			
