<?php
/*******************
 * Header options
 */

Redux::setSection( $opt_name, array(
    'title'            => __( 'Header Options', 'redux-framework-demo' ),
    'id'               => 'header',
    'desc'             => __( 'Header options', 'redux-framework-demo' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );
/* Header top */
Redux::setSection( $opt_name, array(
    'title'            => __( 'Header top bar', 'redux-framework-demo' ),
    'id'               => 'header-top-bar',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        array(
            'id'       => 'headerTopBar-text',
            'type'     => 'textarea',
            'title'    => __( 'Header topbar text', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => "Lorem ipsum dolor sit amet"// 1 = on | 0 = off
        )
    )
) );
/* Header Logo area */
Redux::setSection( $opt_name, array(
    'title'            => __( 'Header Logo area', 'redux-framework-demo' ),
    'id'               => 'header-logo-area',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        array(
            'id'       => 'header-logo-text',
            'type'     => 'text',
            'title'    => __( 'Header logo text', 'redux-framework-demo' ),
            'subtitle' => __( 'Chose a logo for your site in text.', 'redux-framework-demo' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => "リゾート会員ホテル探し.com"// 1 = on | 0 = off
        ),
        array(
            'id'       => 'header-logo-image',
            'type'     => 'media',
            'title'    => __( 'Header logo image', 'redux-framework-demo' ),
            'subtitle' => __( 'Chose a logo for your site in image.', 'redux-framework-demo' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => 0 // 1 = on | 0 = off
        ),
        array(
            'id'       => 'header-logo-banner-text',
            'type'     => 'text',
            'title'    => __( 'Header Logo right banner text', 'redux-framework-demo' ),
            'subtitle' => __( 'Chose a text for your sites banner of logo right.', 'redux-framework-demo' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'header-logo-banner-image',
            'type'     => 'media',
            'title'    => __( 'Header Logo right banner image', 'redux-framework-demo' ),
            'subtitle' => __( 'Chose a image banner for your site for logo right.', 'redux-framework-demo' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
    )
) );

/*******************
 * Home page options
 */

Redux::setSection( $opt_name, array(
    'title'            => __( 'HomePage Options', 'redux-framework-demo' ),
    'id'               => 'HomePage',
    'desc'             => __( 'HomePage options', 'redux-framework-demo' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );
/* homepage map */
Redux::setSection( $opt_name, array(
    'title'            => __( 'HomePage Map Area', 'redux-framework-demo' ),
    'id'               => 'HomePage-map-area',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        // map button one
        array(
            'id'   => 'info-map1',
            'type' => 'info',
            'desc' => __( 'Map button one,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-1',
            'type'     => 'text',
            'title'    => __( 'HomePage map one', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-1-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map one top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-1-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map one left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button two
        array(
            'id'   => 'info-map2',
            'type' => 'info',
            'desc' => __( 'Map button two,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-2',
            'type'     => 'text',
            'title'    => __( 'HomePage map two', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-2-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map two top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-2-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map two left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button three
        array(
            'id'   => 'info-map3',
            'type' => 'info',
            'desc' => __( 'Map button three,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-3',
            'type'     => 'text',
            'title'    => __( 'HomePage map three', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-3-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map three top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-3-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map three left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button four
        array(
            'id'   => 'info-map4',
            'type' => 'info',
            'desc' => __( 'Map button four,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-4',
            'type'     => 'text',
            'title'    => __( 'HomePage map four', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-4-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map two top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-4-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map four left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button five
        array(
            'id'   => 'info-map5',
            'type' => 'info',
            'desc' => __( 'Map button two,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-5',
            'type'     => 'text',
            'title'    => __( 'HomePage map five', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-5-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map five top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-5-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map five left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button six
        array(
            'id'   => 'info-map6',
            'type' => 'info',
            'desc' => __( 'Map button six,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-6',
            'type'     => 'text',
            'title'    => __( 'HomePage map six', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-6-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map six top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-6-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map six left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button seven
        array(
            'id'   => 'info-map7',
            'type' => 'info',
            'desc' => __( 'Map button seven,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-7',
            'type'     => 'text',
            'title'    => __( 'HomePage map seven', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-7-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map two top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-7-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map seven left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button egight
        array(
            'id'   => 'info-map8',
            'type' => 'info',
            'desc' => __( 'Map button eight,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-8',
            'type'     => 'text',
            'title'    => __( 'HomePage map eight', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-8-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map eight top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-8-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map two left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button nine
        array(
            'id'   => 'info-map9',
            'type' => 'info',
            'desc' => __( 'Map button nine,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-9',
            'type'     => 'text',
            'title'    => __( 'HomePage map nine', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-9-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map nine top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-9-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map nine left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button ten
        array(
            'id'   => 'info-map10',
            'type' => 'info',
            'desc' => __( 'Map button ten,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-10',
            'type'     => 'text',
            'title'    => __( 'HomePage map ten', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-10-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map ten top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-10-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map ten left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button eleven
        array(
            'id'   => 'info-map11',
            'type' => 'info',
            'desc' => __( 'Map button eleven,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-11',
            'type'     => 'text',
            'title'    => __( 'HomePage map eleven', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-11-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map eleven top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-11-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map eleven left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // map button twelve
        array(
            'id'   => 'info-map12',
            'type' => 'info',
            'desc' => __( 'Map button twelve,  ', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-map-12',
            'type'     => 'text',
            'title'    => __( 'HomePage map twelve', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-12-posTop',
            'type'     => 'text',
            'title'    => __( 'HomePage map twelve top position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-map-12-posLeft',
            'type'     => 'text',
            'title'    => __( 'HomePage map twelve left position', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => ""// 1 = on | 0 = off
        ),
        // end map fields
    )
) );


/* homepage banner */
Redux::setSection( $opt_name, array(
    'title'            => __( 'HomePage Banner Area', 'redux-framework-demo' ),
    'id'               => 'HomePage-banner-area',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        // banner image and title
        array(
            'id'   => 'info-banner',
            'type' => 'info',
            'desc' => __( 'Homepage banner options.', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-banner-image',
            'type'     => 'upload',
            'title'    => __( 'Homeapge Banner Image', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => get_theme_file_uri('assets/images/cta_banner.jpg') // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-banner-link',
            'type'     => 'upload',
            'title'    => __( 'Homeapge Banner link', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => get_theme_file_uri('assets/images/cta_banner.jpg') // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-banner-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Banner Title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '資料請求で、もれなくダウンロード！ ' // 1 = on | 0 = off
        ),
        // banner button and buttontaglink
        array(
            'id'   => 'info-banner',
            'type' => 'info',
            'desc' => __( 'Homepage banner button options.', 'redux-framework-demo' )
        ),
        array(
            'id'       => 'HomePage-button-text',
            'type'     => 'text',
            'title'    => __( 'Homeapge Button text', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '資料請求する(無料)' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-button-link',
            'type'     => 'text',
            'title'    => __( 'Homeapge Button link', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePage-button-tagline',
            'type'     => 'text',
            'title'    => __( 'Homeapge Button tagline', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => 'かんたん30秒！' // 1 = on | 0 = off
        ),

    ),
));

/* Homepage post loop section */
Redux::setSection( $opt_name, array(
    'title'            => __( 'HomePage Post Area', 'redux-framework-demo' ),
    'id'               => 'HomePage-post-area',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        // postsection one
        array(
            'id'   => 'banner-post-seciton-one',
            'type' => 'info',
            'desc' => __( 'Homepage postarea one', 'redux-framework-demo' )
        ),
        // post section one content
        array(
            'id'       => 'HomePagePostSection-one-show',
            'type'     => 'checkbox',
            'title'    => __( 'Show This section', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '1' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-one-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Postsecton one title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '会員リゾートホテルの賢い購入ノウハウ ' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-one-titleBg',
            'type'     => 'color',
            'title'    => __( 'Homeapge Postsecton one title Background', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'default'  => '#11273A', // 1 = on | 0 = off
            'output'   => array( '.site-title' ),
            'validate' => 'color',
        ),
        array(
            'id'       => 'HomePagePostSection-one-category',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Select category for homesection postarea one', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( '', 'redux-framework-demo' ),
        ),
        // postsection two
        array(
            'id'   => 'banner-post-seciton-two',
            'type' => 'info',
            'desc' => __( 'Homepage postarea two', 'redux-framework-demo' )
        ),
        // post section two content
        array(
            'id'       => 'HomePagePostSection-two-show',
            'type'     => 'checkbox',
            'title'    => __( 'Show This section', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '1' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-two-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Postsecton two title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '会員リゾートホテルの賢い購入ノウハウ ' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-two-titleBg',
            'type'     => 'color',
            'title'    => __( 'Homeapge Postsecton two title Background', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'default'  => '#9E8758', // 1 = on | 0 = off
            'output'   => array( '.site-title' ),
            'validate' => 'color',
        ),
        array(
            'id'       => 'HomePagePostSection-two-category',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Select category for homesection postarea two', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( '', 'redux-framework-demo' ),
        ),
        // postsection three
        array(
            'id'   => 'banner-post-seciton-three',
            'type' => 'info',
            'desc' => __( 'Homepage postarea three', 'redux-framework-demo' )
        ),
        // post section three content
        array(
            'id'       => 'HomePagePostSection-three-show',
            'type'     => 'checkbox',
            'title'    => __( 'Show This section', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '1' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-three-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Postsecton three title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '会員リゾートホテルの賢い購入ノウハウ ' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-three-titleBg',
            'type'     => 'color',
            'title'    => __( 'Homeapge Postsecton three title Background', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'default'  => '#11273A', // 1 = on | 0 = off
            'output'   => array( '.site-title' ),
            'validate' => 'color',
        ),
        array(
            'id'       => 'HomePagePostSection-three-category',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Select category for homesection postarea three', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( '', 'redux-framework-demo' ),
        ),
        // postsection four
        array(
            'id'   => 'banner-post-seciton-four',
            'type' => 'info',
            'desc' => __( 'Homepage postarea four', 'redux-framework-demo' )
        ),
        // post section four content
        array(
            'id'       => 'HomePagePostSection-four-show',
            'type'     => 'checkbox',
            'title'    => __( 'Show This section', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '0' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-four-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Postsecton four title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '会員リゾートホテルの賢い購入ノウハウ ' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-four-titleBg',
            'type'     => 'color',
            'title'    => __( 'Homeapge Postsecton four title Background', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'default'  => '#9E8758', // 1 = on | 0 = off
            'output'   => array( '.site-title' ),
            'validate' => 'color',
        ),
        array(
            'id'       => 'HomePagePostSection-four-category',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Select category for homesection postarea four', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( '', 'redux-framework-demo' ),
        ),
        // postsection five
        array(
            'id'   => 'banner-post-seciton-five',
            'type' => 'info',
            'desc' => __( 'Homepage postarea five', 'redux-framework-demo' )
        ),
        // post section five content
        array(
            'id'       => 'HomePagePostSection-five-show',
            'type'     => 'checkbox',
            'title'    => __( 'Show This section', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '0' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-five-title',
            'type'     => 'text',
            'title'    => __( 'Homeapge Postsecton five title', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => '会員リゾートホテルの賢い購入ノウハウ ' // 1 = on | 0 = off
        ),
        array(
            'id'       => 'HomePagePostSection-five-titleBg',
            'type'     => 'color',
            'title'    => __( 'Homeapge Postsecton five title Background', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'default'  => '#11273A', // 1 = on | 0 = off
            'output'   => array( '.site-title' ),
            'validate' => 'color',
        ),
        array(
            'id'       => 'HomePagePostSection-five-category',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Select category for homesection postarea five', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( '', 'redux-framework-demo' ),
        ),
    )
));
/*******************
 * Footer options
 */

Redux::setSection( $opt_name, array(
    'title'            => __( 'Footer Options', 'redux-framework-demo' ),
    'id'               => 'footer',
    'desc'             => __( 'Footer options', 'redux-framework-demo' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home'
) );
/* Footer options area */
Redux::setSection( $opt_name, array(
    'title'            => __( 'Footer Options', 'redux-framework-demo' ),
    'id'               => 'Footer-options',
    'subsection'       => true,
    'customizer_width' => '450px',
    'fields'           => array(
        array(
            'id'       => 'Footer-para-text',
            'type'     => 'textarea',
            'title'    => __( 'Footer paragraph', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => "◆当サイトに関する注意事項当サイトで提供する商品の情報にあたっては、十分な注意を払って提供しておりますが、情報の正確性その他一切の事項について保証をするものではありません。 ◆お申込みにあたっては、提携事業者のサイトや、利用規約をご確認の上、ご自身でご判断ください。 ◆当社では各商品のサービス内容及びキャンペーン等に関するご質問にはお答えできかねます。提携事業者に直接お問い合わせください。 ◆本ページのいかなる情報により生じた損失に対しても当社は責任を負いません。なお、本注意事項に定めがない事項は当社が定める「利用規約」 が適用されるものとします。" // 1 = on | 0 = off
        ),
        array(
            'id'       => 'Footer-copyright',
            'type'     => 'textarea',
            'title'    => __( 'Footer copyright', 'redux-framework-demo' ),
            'subtitle' => __( '' ),
            'desc'     => __( ' ', 'redux-framework-demo' ),
            'default'  => " © 2019 リゾート会員ホテルナビ.
            " // 1 = on | 0 = off
        ),
    )
) );














/* Radio area will be deleted */
Redux::setSection( $opt_name, array(
    'title'            => __( 'Radio', 'redux-framework-demo' ),
    'id'               => 'basic-Radio',
    'subsection'       => true,
    'customizer_width' => '500px',
    'desc'             => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/radio/" target="_blank">docs.reduxframework.com/core/fields/radio/</a>',
    'fields'           => array(
        array(
            'id'       => 'opt-radio',
            'type'     => 'radio',
            'title'    => __( 'Radio Option', 'redux-framework-demo' ),
            'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                '1' => 'Opt 1',
                '2' => 'Opt 2',
                '3' => 'Opt 3'
            ),
            'default'  => '2'
        ),
        array(
            'id'       => 'opt-radio-data',
            'type'     => 'radio',
            'title'    => __( 'Radio Option w/ Menu Data', 'redux-framework-demo' ),
            'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
            'data'     => 'menu'
        ),
    )
) );