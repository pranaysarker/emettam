jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
	dots: true,
	autoplay:false,
    autoplayTimeout:5000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});


	
jQuery(document).ready(function(){
    
    jQuery(".nav_toggle_tgl").click(function () {
            event.preventDefault();
            jQuery(".nav_area").toggleClass("open");
    });

});

(function($){
    $(document).ready(function(){
        $('.rating-image').on('click',function(){
            $('.hidden-rating-image').show();
        });
    });
}(jQuery));